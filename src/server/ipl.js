const {
    match
} = require("assert");

const fs = require("fs");

function noOfMatchesPlayedPerYear(matches) {
    let yearsList = matches.map(match => {
        return match.season;
    }).filter((year, index, seasonList) => {
        return seasonList.indexOf(year) === index;
    });
    fs.writeFile("files/yearsList.txt", yearsList.join(), (err) => {
        if (err)
            console.log(err);
        else {
            console.log("file yearsList.txt created");
        }
    })
    let matchesPlayedList = [];
    yearsList.forEach(year => {
        let obj = {};
        obj.season = year;
        obj.matchesPlayed = matches.reduce((acc, match) => {
            return match.season === year ? acc += 1 : acc += 0;
        }, 0)
        matchesPlayedList.push(obj);
    })
    fs.writeFile('../public/output/matchesPlayed.json', JSON.stringify(matchesPlayedList), (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file matchesPlayed.json written successfully");
        }
    })
    return matchesPlayedList;
}
exports.noOfMatchesPlayedPerYear = noOfMatchesPlayedPerYear;




function noOfMatchesWonPerTeamPerYear(matches) {
    fs.readFile("./files/yearsList.txt", 'utf8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            console.log("read data from file yearsList.txt");
            let yearsList = data.split(',');
            let teamsList = matches.map(match => {
                return match.team1;
            }).filter((team, index, team1List) => {
                return team1List.indexOf(team) === index;
            })
            fs.writeFile("./files/teamsList.txt", teamsList.join(), (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("file teamsList.txt created successfully");
                    let winsPerSeasonObj = {};
                    yearsList.forEach(year => {
                        winsPerSeasonObj[year] = [];
                        teamsList.forEach(team => {
                            let teamWinsObj = {};
                            teamWinsObj["teamName"] = team;
                            teamWinsObj["wins"] =
                                matches.reduce((winCount, match) => {
                                    if (((match.season === year) && ((match.team1 === team || match.team2 === team) && match.winner === team))) {
                                        return winCount += 1;
                                    } else {
                                        return winCount;
                                    }
                                }, 0)
                            winsPerSeasonObj[year].push(teamWinsObj);
                        })
                    })
                    fs.writeFile('../public/output/winsPerSeasonObj.json', JSON.stringify(winsPerSeasonObj), (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("file winsPerSeasonObj.json written successfully");
                        }
                    })
                    console.log("number of matches won per team per year");

                    console.log(winsPerSeasonObj);
                }
            })

        }
    })

}
exports.noOfMatchesWonPerTeamPerYear = noOfMatchesWonPerTeamPerYear;


function extraRuns2016(deliveries) {
    let minMatchId2016 = 577;
    let maxMatchId2016 = 636;
    // console.log(deliveries);
    let data2016 = deliveries.filter(delivery => {
        return ((Number(delivery.match_id) >= minMatchId2016) && (Number(delivery.match_id) <= maxMatchId2016));
    })
    fs.readFile("./files/teamsList.txt", 'utf8', (err, data) => {
        if (err)
            console.log(err);
        else {
            let teamsList = data.split(',');
            let extraRunsConcededPerTeam = [];
            teamsList.forEach(team => {
                let teamObj = {};
                teamObj.teamName = team;
                teamObj.extraRunsConceded =
                    data2016.reduce((totalRuns, data) => {
                        if ((team === data.bowling_team)) {
                            return (totalRuns += Number(data.extra_runs));
                        } else {
                            return totalRuns;
                        }
                    }, 0)
                extraRunsConcededPerTeam.push(teamObj);
            })
            fs.writeFile('../public/output/extraRunsConcededPerTeam.json', JSON.stringify(extraRunsConcededPerTeam), (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("file extraRunsConcededPerTeam.json written successfully");
                }
            })
            console.log("Extra runs conceded per team in year 2016")
            console.log(extraRunsConcededPerTeam);
        }
    })
}
exports.extraRuns2016 = extraRuns2016;

function economicalBowlers2015(deliveries) {
    let bowlersList = deliveries.map(delivery => {
        return delivery.bowler;
    }).filter((bowler, index, bowlerArray) => {
        return bowlerArray.indexOf(bowler) === index;
    })

    let minId2015 = 518;
    let maxId2015 = 576;
    let data2015 = deliveries.filter(delivery => {
        return (Number(delivery.match_id) >= minId2015 && Number(delivery.match_id) <= maxId2015);
    })

    let bowlerEconomies = [];
    bowlersList.forEach(bowler => {
        let bowlerEconomyObj = {};
        bowlerEconomyObj.bowlerName = bowler;
        bowlerEconomyObj.runsConceded = data2015.reduce((totalRunsConceded, data) => {
            if ((data.bowler === bowler && (data.bye_runs == 0 && data.legbye_runs == 0))) {
                return (totalRunsConceded += Number(data.total_runs));
            } else {
                return (totalRunsConceded += 0);
            }
        }, 0)
        bowlerEconomyObj.ballsDelivered = data2015.reduce((totalBallsDelivered, data) => {
            if ((data.bowler === bowler && (data.wide_runs == 0 && data.noball_runs == 0))) {
                return (totalBallsDelivered += 1);
            } else {
                return (totalBallsDelivered);
            }
        }, 0)
        bowlerEconomyObj.overs = Math.floor(bowlerEconomyObj.ballsDelivered / 6);
        bowlerEconomyObj.economy = Number((bowlerEconomyObj.runsConceded / bowlerEconomyObj.overs).toFixed(2));
        (bowlerEconomyObj.overs === 0) ? (bowlerEconomyObj.economy = 0) : (bowlerEconomyObj.economy = bowlerEconomyObj.economy);
        if (bowlerEconomyObj.overs != 0) {
            bowlerEconomies.push(bowlerEconomyObj);
        }
    })
    bowlerEconomies.sort((a, b) => {
        return a.economy - b.economy;
    })
    let top10BowlerEconomies = [];
    for (let i = 0; i < 10; i++) {
        top10BowlerEconomies.push(bowlerEconomies[i]);
    }
    fs.writeFile('../public/output/top10EconomicalBowlers.json', JSON.stringify(top10BowlerEconomies), (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file top10EconomicalBowlers.json written successfully");
        }
    })
    return top10BowlerEconomies;
}
exports.economicalBowlers2015 = economicalBowlers2015;

function tossAndWon(matches) {
    fs.readFile('./files/teamsList.txt', 'utf8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let teamTossAndWinsList = [];
            let teamsList = data.split(',');
            teamsList.map(team => {
                let teamObj = {};
                teamObj.teamName = team;
                teamObj.times = matches.reduce((totalTimes, match) => {
                    if ((team === match.toss_winner && team === match.winner)) {
                        return (totalTimes += 1);
                    } else {
                        return totalTimes;
                    }
                }, 0);
                teamTossAndWinsList.push(teamObj);
            })
            fs.writeFile('../public/output/tossAndMatchWins.json', JSON.stringify(teamTossAndWinsList), (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("file tossAndMatchWins.json written successfully");
                }
            })
            console.log("number of times a team won the toss and also won the match");
            console.log(teamTossAndWinsList);
        }
    })
}
exports.tossAndWon = tossAndWon;

function highestPOTMWinsPerSeason(matches) {
    fs.readFile('./files/yearsList.txt', 'utf8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let yearsList = data.split(',');
            let POTMList = matches.map(match => {
                return match.player_of_match;
            }).filter((player, index, POTMList) => {
                return POTMList.indexOf(player) === index;
            })
            let POTMObj = {};
            let mostPOTMWinsList = [];
            yearsList.forEach(year => {
                POTMObj[year] = [];
                POTMList.map(player => {
                    let temp = {};
                    temp.playerName = player;
                    temp.timesWon = matches.reduce((totalTimes, match) => {
                        if (year === match.season && player === match.player_of_match) {
                            return totalTimes += 1;
                        } else {
                            return totalTimes;
                        }
                    }, 0);
                    POTMObj[year].push(temp);
                })
                POTMObj[year].sort((a, b) => {
                    return Number(b.timesWon) - Number(a.timesWon);
                })
                mostPOTMWinsList.push(POTMObj[year][0]);
            })
            fs.writeFile('../public/output/mostPOTMWins.json', JSON.stringify(mostPOTMWinsList), (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("file mostPOTMWins.json written successfully");
                }
            })
            console.log("Highest number of player of the match awards won by a player per season");
            console.log(mostPOTMWinsList);
        }
    })
}
exports.highestPOTMWinsPerSeason = highestPOTMWinsPerSeason;

function strikeRateOfAbatsmanPerSeason(deliveries) {
    let targetBatsman = "DA Warner";
    let match_ids = [{
            year: '2008',
            min_id: 60,
            max_id: 117
        },
        {
            year: '2009',
            min_id: 118,
            max_id: 174
        },
        {
            year: '2010',
            min_id: 175,
            max_id: 234
        },
        {
            year: '2011',
            min_id: 235,
            max_id: 307
        },
        {
            year: '2012',
            min_id: 308,
            max_id: 381
        },
        {
            year: '2013',
            min_id: 382,
            max_id: 457
        },
        {
            year: '2014',
            min_id: 458,
            max_id: 517
        },
        {
            year: '2015',
            min_id: 518,
            max_id: 576
        },
        {
            year: '2016',
            min_id: 577,
            max_id: 636
        },
        {
            year: '2017',
            min_id: 1,
            max_id: 59
        }
    ]
    let strikeRatePerYearList = [];
    match_ids.map(match_id => {
        let strikeRateObj = {};
        strikeRateObj.year = match_id.year;
        let dataByYear = deliveries.filter(delivery => {
            return (Number(delivery.match_id) >= match_id.min_id && Number(delivery.match_id) <= match_id.max_id) && (delivery.batsman === targetBatsman);
        })
        let runs = dataByYear.reduce((totalRuns, data) => {
            return totalRuns += Number(data.total_runs);
        }, 0);
        let ballsFaced = dataByYear.reduce((totalBalls, data) => {
            if (data.wide_runs == 0 && data.noball_runs == 0) {
                return totalBalls += 1;
            } else {
                return totalBalls += 0;
            }
        }, 0);
        if (ballsFaced === 0) {
            strikeRateObj.strikeRate = 0;
        } else {
            strikeRateObj.strikeRate = ((runs / ballsFaced) * 100).toFixed(2);
        }
        strikeRatePerYearList.push(strikeRateObj);
    })
    fs.writeFile('../public/output/strikeRateOfBatsman.json', JSON.stringify(strikeRatePerYearList), (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file strikeRateOfBatsman.json written successfully");
        }
    })
    console.log("strike rate of a batsman for each season");
    return strikeRatePerYearList;
}
exports.strikeRateOfAbatsmanPerSeason = strikeRateOfAbatsmanPerSeason;

function highestDismissals(deliveries) {
    let dismissalStats = deliveries.filter(delivery => {
        return delivery.player_dismissed != '';
    })
    let dismissedPlayers = dismissalStats.map(dismissedStat => {
        return dismissedStat.player_dismissed;
    }).filter((player, index, dismissedList) => {
        return dismissedList.indexOf(player) === index;
    });
    let dismissingPlayers = dismissalStats.map(dismissedStat => {
        if (dismissedStat.dismissal_kind != '') {
            if (!(dismissedStat.dismissal_kind === 'caught' || dismissedStat.dismissal_kind == 'run out')) {
                return dismissedStat.bowler;
            }
        }
    }).filter((player, index, dismissingList) => {
        return dismissingList.indexOf(player) === index;
    })
    let dismissalList = [];
    for (let i = 0; i < dismissedPlayers.length; i++) {
        let dismissed = dismissedPlayers[i];
        for (let j = 0; j < dismissingPlayers.length; j++) {
            let dismissing = dismissingPlayers[j];
            let dismissalObj = {};
            dismissalObj.dismissed = dismissed;
            dismissalObj.dismissedBy = dismissing;
            let count = 0;
            for (let k = 0; k < dismissalStats.length; k++) {
                if (!(dismissalStats[k].dismissal_kind === 'caught' || dismissalStats[k].dismissal_kind === 'run out')) {
                    if (dismissed === dismissalStats[k].player_dismissed && (dismissing === dismissalStats[k].bowler)) {
                        count++;
                    }
                }
            }
            if (count == 0) {
                continue;
            }
            dismissalObj.times = count;
            dismissalList.push(dismissalObj);
        }
    }
    dismissalList.sort((a, b) => {
        return b.times - a.times;
    })
    let highestDismissedPlayer = dismissalList[0];
    fs.writeFile('../public/output/highestDismissal.json', JSON.stringify(highestDismissedPlayer), (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file highestDismissal.json written successfully");
        }
    })
    return highestDismissedPlayer;
}
exports.highestDismissals = highestDismissals;



function bestEconomyInSuperOvers(deliveries) {
    let superOverStats = deliveries.filter(delivery => {
        return delivery.is_super_over == 1;
    });
    let bowlersList = superOverStats.map(superOverStat => {
        return superOverStat.bowler;
    }).filter((bowler, index, bowlersArray) => {
        return bowlersArray.indexOf(bowler) === index;
    });
    let bowlerEconomiesList = [];
    bowlersList.map(bowler => {
        let temp = {};
        temp.bowler = bowler;
        let runsConceded = superOverStats.reduce((totalConceded, superOverStat) => {
            if (superOverStat.bowler === bowler) {
                return totalConceded += Number(superOverStat.total_runs);
            } else {
                return totalConceded;
            }
        }, 0);
        let ballsDelivered = superOverStats.reduce((totalBalls, superOverStat) => {
            if (superOverStat.bowler === bowler && (superOverStat.noball_runs == 0 && superOverStat.wide_runs == 0)) {
                return totalBalls += 1;
            } else {
                return totalBalls;
            }
        }, 0)
        let overs = (ballsDelivered / 6);
        let economy = (runsConceded / overs).toFixed(2);
        temp.economy = economy;
        bowlerEconomiesList.push(temp);
    })
    bowlerEconomiesList.sort((a, b) => {
        return a.economy - b.economy;
    })
    let mostEconomicalInSuperOvers = bowlerEconomiesList[0].bowler;
    fs.writeFile('../public/output/mostEconomicalInSuperOvers.json', JSON.stringify(mostEconomicalInSuperOvers), (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file mostEconomicalInSuperOvers.json written successfully");
        }
    })
    return mostEconomicalInSuperOvers;

}
exports.bestEconomyInSuperOvers = bestEconomyInSuperOvers;