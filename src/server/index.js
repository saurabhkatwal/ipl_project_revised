let mainObj = require("./ipl");
let noOfMatchesPlayedPerYear = mainObj.noOfMatchesPlayedPerYear;
let noOfMatchesWonPerTeamPerYear = mainObj.noOfMatchesWonPerTeamPerYear;
let extraRuns2016 = mainObj.extraRuns2016;
let economicalBowlers2015 = mainObj.economicalBowlers2015;
let tossAndWon = mainObj.tossAndWon;
let highestPOTMWinsPerSeason=mainObj.highestPOTMWinsPerSeason;
let strikeRateOfAbatsmanPerSeason=mainObj.strikeRateOfAbatsmanPerSeason;
let bestEconomyInSuperOvers=mainObj.bestEconomyInSuperOvers;
let highestDismissals=mainObj.highestDismissals;
let matchesPath = ("../data/matches.csv");
let deliveriesPath = ("../data/deliveries.csv");
const csv = require("csvtojson");

function main() {
    csv().fromFile(matchesPath)
        .then(matches => {
            let resNoOfMatchesPlayedPerYear = noOfMatchesPlayedPerYear(matches);
            console.log("number of matches played per year");
            console.log(resNoOfMatchesPlayedPerYear);
        })

    
    csv().fromFile(matchesPath)
        .then(matches => {
            let resNoOfMatchesWonPerTeamPerYear = noOfMatchesWonPerTeamPerYear(matches);
            console.log("")
            console.log(resNoOfMatchesWonPerTeamPerYear);
        })

    csv().fromFile(deliveriesPath)
        .then(deliveries => {
            let resExtraRuns2016 = extraRuns2016(deliveries);
            console.log(resExtraRuns2016);
        })

    csv().fromFile(deliveriesPath)
        .then(deliveries => {
            let resEconomicalBowlers2015 = economicalBowlers2015(deliveries);
            console.log("top 10 economical bowlers in 2015");
            console.log(resEconomicalBowlers2015);
        })

    csv().fromFile(matchesPath)
        .then(matches => {
            let resTossAndWon = tossAndWon(matches);
            console.log(resTossAndWon);
        })

    csv().fromFile(matchesPath)
        .then(matches => {
            let resHighestPOTMWinsPerSeason=highestPOTMWinsPerSeason(matches);
            console.log(resHighestPOTMWinsPerSeason);
        })

    csv().fromFile(deliveriesPath)
        .then(deliveries => {
            let resStrikeRateOfAbatsmanPerSeason=strikeRateOfAbatsmanPerSeason(deliveries);
            console.log(resStrikeRateOfAbatsmanPerSeason);
        })

    csv().fromFile(deliveriesPath)
        .then(deliveries => {
            let resBestEconomyInSuperOvers=bestEconomyInSuperOvers(deliveries);
            console.log("best economy in super overs");
            console.log(resBestEconomyInSuperOvers);
        })

    csv().fromFile(deliveriesPath)
        .then(deliveries => {
            let resHighestDismissals=highestDismissals(deliveries);
            console.log("highest dismissal of a player by another player");
            console.log(resHighestDismissals);
        })

}
main();